﻿<%@ Page Title="Schedule Events" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="EventSchedule.aspx.cs" Inherits="Placement_Portal.EventSchedule" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="form1" runat="server">
        <asp:Label ID="Label1" runat="server" Text="Select a Date : "></asp:Label>
        <br />
        <asp:Calendar ID="Calendar1" runat="server"></asp:Calendar>
        <br />
        <asp:Label ID="Label2" runat="server" Text="Select a time :"></asp:Label>
        <br />
        <br />
        <asp:TextBox ID="TimeTextBox" runat="server" Width="250px" TextMode="Time"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="Label3" runat="server" Text="Event ID"></asp:Label>
        <br />
        <br />
        <asp:TextBox ID="EventIDTextBox" runat="server" Width="250px" ></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="Label4" runat="server" Text="Company ID"></asp:Label>
        <br />
        <br />
        <asp:TextBox ID="CompanyIDTextBox" runat="server" Width="250px" ></asp:TextBox>
        <br />
        <br />
        <br />
        <%--<asp:Label ID="Label5" runat="server" Text="Event Description"></asp:Label>--%>
        <br />
        <br />
        <asp:TextBox ID="EventDescriptionTextBox" runat="server" Width="252px" ></asp:TextBox>
        <br />
        <br />
        <br />
        <br />
        <asp:Button ID="CreateEventButton" runat="server" Text="Create an Event" Width="130px" OnClick="CreateEventButton_OnClick" />
        <br />
        <asp:Label ID="Label5" runat="server"></asp:Label>
        <br />
        <br />
        <br />
        <br />
        <asp:Button ID="BackButton" runat="server" Text="Back" OnClick="BackButton_OnClick" />
        <br />
    </form>
</asp:Content>
