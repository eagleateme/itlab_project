﻿<%@ Page Title="View Details" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="View_Details.aspx.cs" Inherits="Placement_Portal.View_Details" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="form1" runat="server">
        <div id = "Personal_Details" runat="server">
            <asp:Label ID="PersonalDetails_Label" Text="Personal Details" runat="server" Font-Bold="True" Font-Overline="False" Font-Underline="True"></asp:Label>
            <br />
&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <br />
             <asp:Table ID="Personal_Details_Table" runat="server" CellSpacing = "1" Height="135px" Width="906px">
                 <asp:TableRow><asp:TableHeaderCell Text =" Reg.No : "> </asp:TableHeaderCell><asp:TableCell><asp:TextBox ReadOnly="true"  ID="RegNoTextBox" runat="server" CssClass="txtboxstyle"></asp:TextBox>
                 </asp:TableCell><asp:TableHeaderCell Text =" Name : "> </asp:TableHeaderCell><asp:TableCell><asp:TextBox ReadOnly="true"  ID="NameTextBox" runat="server" CssClass="txtboxstyle"></asp:TextBox></asp:TableCell></asp:TableRow>
                 <asp:TableRow><asp:TableHeaderCell Text =" Gender : "> </asp:TableHeaderCell><asp:TableCell><asp:TextBox ReadOnly="true"  ID="GenderTextBox" runat="server" CssClass="txtboxstyle"></asp:TextBox></asp:TableCell><asp:TableHeaderCell Text =" DOB : "> </asp:TableHeaderCell><asp:TableCell><asp:TextBox ReadOnly="true"  ID="DOBTextBox" runat="server" CssClass="txtboxstyle"></asp:TextBox></asp:TableCell></asp:TableRow>
                 <asp:TableRow><asp:TableHeaderCell Text =" Email  : "> </asp:TableHeaderCell><asp:TableCell><asp:TextBox ReadOnly="true"  ID="EmailTextBox" runat="server" CssClass="txtboxstyle"></asp:TextBox></asp:TableCell><asp:TableHeaderCell Text ="Ph.No : "> </asp:TableHeaderCell><asp:TableCell><asp:TextBox ReadOnly="true"  ID="PhoneTextBox" runat="server" CssClass="txtboxstyle"></asp:TextBox></asp:TableCell></asp:TableRow>
            </asp:Table>
       </div>
        <br />
        <div id = "Academic Details">
            <asp:Label ID="Academic_Details_Label" Text="Academic Details" runat="server" Font-Bold="True" Font-Overline="False" Font-Underline="True"></asp:Label>
            <br />
            <asp:Table ID="Academic_Details_Table1" runat="server" CellSpacing="1" Height="140px" Width="851px">
            <asp:TableRow><asp:TableHeaderCell Text="Xth Standard" Width= "150px"></asp:TableHeaderCell><asp:TableHeaderCell  Width= "150px" Text="XIIth Standard"></asp:TableHeaderCell><asp:TableHeaderCell  Width= "150px" Text="Backlogs"></asp:TableHeaderCell></asp:TableRow>
            <asp:TableRow><asp:TableCell><asp:TextBox ReadOnly="true"   Width= "150px" ID="XthStandardTextBox" runat="server" CssClass="txtboxstyle"></asp:TextBox></asp:TableCell><asp:TableCell><asp:TextBox ReadOnly="true"   Width= "150px" ID="XIIthStandardTextBox" runat="server" CssClass="txtboxstyle"></asp:TextBox></asp:TableCell><asp:TableCell><asp:TextBox ReadOnly="true"   Width= "150px" ID="BacklogsTextBox" runat="server" CssClass="txtboxstyle"></asp:TextBox></asp:TableCell></asp:TableRow>
            </asp:Table>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <br />
            <br />
             <asp:Table ID="Academic_Details_Table2" runat="server" CellSpacing="1" Height="127px" Width="851px">
              <asp:TableRow><asp:TableHeaderCell Text="Semester" Width="100px"></asp:TableHeaderCell><asp:TableHeaderCell Text="Sem1" Width="50px"></asp:TableHeaderCell><asp:TableHeaderCell Text="Sem2" Width="50px"></asp:TableHeaderCell><asp:TableHeaderCell Text="Sem3" Width="50px"></asp:TableHeaderCell><asp:TableHeaderCell Text="Sem4" Width="50px"></asp:TableHeaderCell><asp:TableHeaderCell Text="Sem5" Width="50px"></asp:TableHeaderCell><asp:TableHeaderCell Text="Sem6" Width="50px"></asp:TableHeaderCell><asp:TableHeaderCell Text="Sem7" Width="50px"></asp:TableHeaderCell><asp:TableHeaderCell Text="Sem8" Width="50px"></asp:TableHeaderCell></asp:TableRow>
              <asp:TableRow><asp:TableHeaderCell Text="GPA     " Width="100px"></asp:TableHeaderCell><asp:TableCell><asp:TextBox ReadOnly="true"   Width= "50px" ID="SemTextBox1" runat="server" CssClass="txtboxstyle" ></asp:TextBox></asp:TableCell><asp:TableCell><asp:TextBox ReadOnly="true"   Width= "50px" ID="SemTextBox2"  runat="server" CssClass="txtboxstyle"></asp:TextBox></asp:TableCell><asp:TableCell><asp:TextBox ReadOnly="true"   Width= "50px" ID="SemTextBox3"  runat="server" CssClass="txtboxstyle"></asp:TextBox></asp:TableCell><asp:TableCell><asp:TextBox ReadOnly="true"   Width= "50px" ID="SemTextBox4"  runat="server" CssClass="txtboxstyle"></asp:TextBox></asp:TableCell><asp:TableCell><asp:TextBox ReadOnly="true"   Width= "50px" ID="SemTextBox5" runat="server"   CssClass="txtboxstyle"></asp:TextBox></asp:TableCell><asp:TableCell><asp:TextBox ReadOnly="true"  Width= "50px" ID="SemTextBox6"  runat="server" CssClass="txtboxstyle"></asp:TextBox></asp:TableCell><asp:TableCell><asp:TextBox ReadOnly="true"   Width= "50px" ID="SemTextBox7" runat="server" CssClass="txtboxstyle"></asp:TextBox></asp:TableCell><asp:TableCell><asp:TextBox ReadOnly="true"   Width= "50px" ID="SemTextBox8" runat="server" CssClass="txtboxstyle"></asp:TextBox></asp:TableCell></asp:TableRow>  
             </asp:Table>
            </div>
        <br />
        &nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;
        <br />
        <br />
       <div id = "Extra Details">
            <asp:Label ID="Extra_Details_Label" Text="Extra Details" runat="server" Font-Bold="True" Font-Overline="False" Font-Underline="True"></asp:Label>
            <br />
            <br />
            <br />
            <asp:Table ID="Extra_Details_Table" runat="server" CellSpacing="1" Width="864px" Height="290px">
                <asp:TableHeaderRow><asp:TableHeaderCell Text="Mother's Name"></asp:TableHeaderCell><asp:TableCell><asp:TextBox ReadOnly="true"  ID="MotherNameTextBox" runat="server"></asp:TextBox></asp:TableCell><asp:TableHeaderCell Text="Father's Name"></asp:TableHeaderCell><asp:TableCell><asp:TextBox ReadOnly="true"  ID="FatherNameTextBox" runat="server"></asp:TextBox></asp:TableCell></asp:TableHeaderRow>
                <asp:TableHeaderRow><asp:TableHeaderCell Text="Mother's Occupation"></asp:TableHeaderCell><asp:TableCell><asp:TextBox ReadOnly="true"  ID="MotherOccupationTextBox" runat="server"></asp:TextBox></asp:TableCell><asp:TableHeaderCell Text="Father's Occupation"></asp:TableHeaderCell><asp:TableCell><asp:TextBox ReadOnly="true"  ID="FatherOccupationTextBox" runat="server"></asp:TextBox></asp:TableCell></asp:TableHeaderRow>
                <asp:TableHeaderRow><asp:TableHeaderCell Text="Xth School"></asp:TableHeaderCell><asp:TableCell><asp:TextBox ReadOnly="true"  ID="XSchoolTextBox" runat="server"></asp:TextBox></asp:TableCell><asp:TableHeaderCell Text="Xth Board"></asp:TableHeaderCell><asp:TableCell><asp:TextBox ReadOnly="true"  ID="XBoardTextBox" runat="server"></asp:TextBox></asp:TableCell></asp:TableHeaderRow>
                <asp:TableHeaderRow><asp:TableHeaderCell Text="XIIth School"></asp:TableHeaderCell><asp:TableCell><asp:TextBox ReadOnly="true"  ID="XIISchoolTextBox" runat="server"></asp:TextBox></asp:TableCell><asp:TableHeaderCell Text="XIIth Board"></asp:TableHeaderCell><asp:TableCell><asp:TextBox ReadOnly="true"  ID="XIIBoardTextBox" runat="server"></asp:TextBox></asp:TableCell></asp:TableHeaderRow>
                <asp:TableHeaderRow><asp:TableHeaderCell Text="Citizenship"></asp:TableHeaderCell><asp:TableCell><asp:TextBox ReadOnly="true"  ID="CitizenTextBox" runat="server"></asp:TextBox></asp:TableCell><asp:TableHeaderCell Text="State"></asp:TableHeaderCell><asp:TableCell><asp:TextBox ReadOnly="true"  ID="StateTextBox" runat="server"></asp:TextBox></asp:TableCell></asp:TableHeaderRow>
                <asp:TableHeaderRow><asp:TableHeaderCell Text="City"></asp:TableHeaderCell><asp:TableCell><asp:TextBox ReadOnly="true"  ID="CityTextBox" runat="server"></asp:TextBox></asp:TableCell><asp:TableHeaderCell Text="Address"></asp:TableHeaderCell><asp:TableCell><asp:TextBox ReadOnly="true"  ID="AddressTextBox" runat="server"></asp:TextBox></asp:TableCell></asp:TableHeaderRow>
                <asp:TableHeaderRow><asp:TableHeaderCell Text="Aadhaar"></asp:TableHeaderCell><asp:TableCell><asp:TextBox ReadOnly="true"  ID="AadhaarTextBox" runat="server"></asp:TextBox></asp:TableCell></asp:TableHeaderRow>
           <asp:TableHeaderRow><asp:TableHeaderCell Text="Branch"></asp:TableHeaderCell><asp:TableCell><asp:TextBox ReadOnly="true"  ID="BranchTextBox" runat="server"></asp:TextBox></asp:TableCell></asp:TableHeaderRow>
                </asp:Table>
        </div>
        <br />
        <div style="margin-left: 40px">
        </div>
        <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <br />
        <br />
        <asp:Button ID="BackButton" runat="server" Text="Back" OnClick="BackButton_OnClick" />
        <br />
    </form>
</asp:Content>
