﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.DynamicData;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.SqlTypes;


namespace Placement_Portal
{
    public partial class EditResume : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            Page.Theme = (string)Session["Theme"];
        }
        //Add a QueryString kind of thing 
        int curr_id;

        protected int NumberOfControlsI
        {
            get { return Convert.ToInt32(Session["noConI"]); }
            set { Session["noConI"] = value.ToString(); }
        }

        protected int NumberOfControlsP
        {
            get { return Convert.ToInt32(Session["noConP"]); }
            set { Session["noConP"] = value.ToString(); }
        }

        private void Page_Init(object sender, System.EventArgs e)
        {
            curr_id = (int)Session["username"];

            if (!Page.IsPostBack)
            {
                //Initiate the counter of dynamically added controls
                this.NumberOfControlsP = 0;
                this.NumberOfControlsI = 0;
            }
            else
            { 
                //Controls must be repeatedly be created on postback
                this.createControlsI();
                this.createControlsP();
            }
        }

        protected void addButton1_Click(object sender, EventArgs e)
        {
            TextBox tbx = new TextBox();
            tbx.ID = "TextBoxInternship" + NumberOfControlsI;
            tbx.CssClass = "wide";
            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source = (localdb)\MSSQLlocalDB; Initial Catalog = Placement_Database; Integrated Security = True";
            SqlCommand cmd = new SqlCommand("INSERT into Student_Internship(Student_ID, Internship_ID, Internship_Description) VALUES (@student_id, @internship_id, @int_desc)",con);
            cmd.Parameters.AddWithValue("@student_id", curr_id);
            cmd.Parameters.AddWithValue("@internship_id",NumberOfControlsI+1);
            cmd.Parameters.AddWithValue("@int_desc", "none");

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();

            } catch(Exception err)
            {
                Label3.Text = err.ToString();
            }
            finally
            {
                con.Close();
            }
            NumberOfControlsI++;

            PlaceHolder1.Controls.Add(tbx);
        }



        protected void addButton2_Click(object sender, EventArgs e)
        {
            TextBox tbx = new TextBox();
            tbx.ID = "TextBoxProject" + NumberOfControlsP;
            tbx.CssClass = "wide";
            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source = (localdb)\MSSQLlocalDB; Initial Catalog = Placement_Database; Integrated Security = True";
            SqlCommand cmd = new SqlCommand("INSERT into Student_Project(Student_ID, Project_ID, Project_Description) VALUES (@student_id, @proj_id, @proj_desc)", con);
            cmd.Parameters.AddWithValue("@student_id", curr_id);
            cmd.Parameters.AddWithValue("@proj_id", NumberOfControlsP + 1);
            cmd.Parameters.AddWithValue("@proj_desc", "none");

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();

            }
            catch (Exception err)
            {
                Label3.Text = err.ToString();
            }
            finally
            {
                con.Close();
            }
            NumberOfControlsP++;

            PlaceHolder2.Controls.Add(tbx);
        }


        private void createControlsI()
        {
            int count = this.NumberOfControlsI;

            for (int i = 0; i < count; i++)
            {
                TextBox tx = new TextBox();
                tx.ID = "TextBoxInternship" + i.ToString();
                tx.CssClass = "wide";

                //Add the Controls to the container of your choice
                PlaceHolder1.Controls.Add(tx);
            }
        }

        private void createControlsP()
        {
            int count = this.NumberOfControlsP;

            for (int i = 0; i < count; i++)
            {
                TextBox tx = new TextBox();
                tx.ID = "TextBoxProject" + i.ToString();
                tx.CssClass = "wide";

                //Add the Controls to the container of your choice
                PlaceHolder2.Controls.Add(tx);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                SqlConnection con = new SqlConnection();
                con.ConnectionString = @"Data Source = (localdb)\MSSQLlocalDB; Initial Catalog = Placement_Database ; Integrated Security = True ;";
                try
                {
                    con.Open();

                    //Command to update single-valued entries
                    SqlCommand cmd = new SqlCommand("SELECT * FROM Student_Details WHERE Student_ID = @student_id", con);
                    cmd.Parameters.AddWithValue("@student_id", curr_id);
                    SqlDataReader reader = cmd.ExecuteReader();
                    reader.Read();
                    TextBoxObjective.Text = reader["Objective"].ToString();
                    TextBoxAreasOfI.Text = reader["Areas_of_Interest"].ToString();
                    TextBoxLang.Text = reader["Languages"].ToString();
                    TextBoxSkills.Text = reader["Technical_Skills"].ToString();
                    TextBoxOnlineCourse.Text = reader["Online_Courses"].ToString();
                    TextBoxSocVol.Text = reader["Social_Responsibilities"].ToString();
                    TextBoxElec.Text = reader["Electives"].ToString();
                    TextBoxExtraCAct.Text = reader["Extra_Curricular"].ToString();
                    TextBoxAchieve.Text = reader["Achievements"].ToString();
                    reader.Close();

                    //Command to get number of internships
                    cmd.CommandText = "SELECT COUNT(*) FROM Student_Internship WHERE Student_ID = @student_id";
                    Int32 internship_Count = (Int32)cmd.ExecuteScalar();
                    this.NumberOfControlsI = internship_Count;

                    //Command to fetch internships of student
                    if (internship_Count > 0)
                    {
                        cmd.CommandText = "SELECT * FROM Student_Internship WHERE Student_ID = @student_id";
                        reader = cmd.ExecuteReader();

                        for (int i = 0; i < internship_Count; i++)
                        {
                            reader.Read();
                            TextBox tx = new TextBox();
                            tx.ID = "TextBoxInternship" + i.ToString();
                            tx.CssClass = "wide";
                            tx.Text = reader["Internship_Description"].ToString();
                            //Add the Controls to the container of your choice
                            PlaceHolder1.Controls.Add(tx);
                        }
                        reader.Close();
                    }

                    //Command to get number of projects
                    cmd.CommandText = "SELECT COUNT(*) FROM Student_Project WHERE Student_ID = @student_id";
                    Int32 project_Count = (Int32)cmd.ExecuteScalar();
                    this.NumberOfControlsP = project_Count;

                    //Command to fetch projects of student
                    if (project_Count > 0)
                    {
                        cmd.CommandText = "SELECT * FROM Student_Project WHERE Student_ID = @student_id";
                        reader = cmd.ExecuteReader();

                        for (int i = 0; i < project_Count; i++)
                        {
                            reader.Read();
                            TextBox tx = new TextBox();
                            tx.ID = "TextBoxProject" + i.ToString();
                            tx.CssClass = "wide";
                            tx.Text = reader["Project_Description"].ToString();
                            //Add the Controls to the container of your choice
                            PlaceHolder2.Controls.Add(tx);
                        }
                        reader.Close();
                    }

                }
                catch (Exception err)
                {
                    Label3.Text = err.ToString();
                }
                finally
                {
                    con.Close();
                }
            }
           
        }

        protected void SubmitFormButton_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source = (localdb)\MSSQLlocalDB; Initial Catalog = Placement_Database; Integrated Security = True";
            SqlCommand cmd = new SqlCommand("UPDATE Student_Details SET Objective = @objective, Areas_of_Interest = @aoi, Languages = @lang, Technical_Skills= @skills, Online_Courses = @online_courses, Social_Responsibilities = @social, Electives = @electives, Extra_Curricular = @extra, Achievements = @achievements WHERE Student_ID = @student_id", con);

            cmd.Parameters.AddWithValue("@student_id", curr_id);
            cmd.Parameters.AddWithValue("@objective", TextBoxObjective.Text.ToString());
            cmd.Parameters.AddWithValue("@aoi", TextBoxAreasOfI.Text);
            cmd.Parameters.AddWithValue("@lang", TextBoxLang.Text);
            cmd.Parameters.AddWithValue("@skills", TextBoxSkills.Text);
            cmd.Parameters.AddWithValue("@online_courses", TextBoxOnlineCourse.Text);
            cmd.Parameters.AddWithValue("@social", TextBoxSocVol.Text);
            cmd.Parameters.AddWithValue("@electives", TextBoxElec.Text);
            cmd.Parameters.AddWithValue("@extra", TextBoxExtraCAct.Text);
            cmd.Parameters.AddWithValue("@achievements", TextBoxAchieve.Text);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                Label1.Text += this.NumberOfControlsI.ToString() + "  " + this.NumberOfControlsP.ToString();

                //Defining Intial Count for the Internship and Project TextBoxes
                int initialICount = this.NumberOfControlsI;
                int initialPCount = this.NumberOfControlsP;

                //Updating Student_Internship
                int i;
                for(i = 0; i < initialICount; i++)
                {
                    // Find control on page.
                    TextBox tempTextBox = (TextBox) resumeForm.FindControl("TextBoxInternship" + i.ToString());
                    cmd.CommandText = "UPDATE Student_Internship SET Internship_Description = @int_desc WHERE Student_ID = @student_id AND Internship_ID = @internship_id";

                    //Clearing, So that looping can work
                    cmd.Parameters.Clear();

                    cmd.Parameters.AddWithValue("@student_id", curr_id);
                    cmd.Parameters.AddWithValue("@int_desc", tempTextBox.Text.ToString());
                    cmd.Parameters.AddWithValue("@internship_id", i + 1);

                    cmd.ExecuteNonQuery();
                }

                //Updating Student_Project
                for (i = 0; i < initialPCount; i++)
                {
                    // Find control on page.
                    TextBox tempTextBox = (TextBox)resumeForm.FindControl("TextBoxProject" + i.ToString());
                    cmd.CommandText = "UPDATE Student_Project SET Project_Description = @proj_desc WHERE Student_ID = @student_id AND Project_ID = @proj_id";
                    
                    //Clearing Parameters, So that looping can work
                    cmd.Parameters.Clear();

                    cmd.Parameters.AddWithValue("@student_id", curr_id);
                    cmd.Parameters.AddWithValue("@proj_desc", tempTextBox.Text.ToString());
                    cmd.Parameters.AddWithValue("@proj_id", i + 1);

                    cmd.ExecuteNonQuery();
                    Label3.Text = "Succesfull";

                }
            }
            catch (Exception err)
            {
                //Debugging Purposes
                Label13.Text = err.ToString();
            }
            finally
            {
                
                con.Close();
            }
        }
        protected void BackButton_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("StudentMain.aspx");
        }
    }
}