﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.DynamicData;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace Placement_Portal
{
    public partial class CreateProfile : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            Page.Theme = (string)Session["Theme"];
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source = (localdb)\MSSQLlocalDB; Initial Catalog = Placement_Database; Integrated Security = True";
            SqlCommand cmd = new SqlCommand("INSERT into Student_Details(Student_ID,Name,Gender,Email,DOB,Contact_Number,Tenth_Marks,Twelfth_Marks,Backlogs,Sem1_Grade,Sem2_Grade,Sem3_Grade,Sem4_Grade,Sem5_Grade,Sem6_Grade,Sem7_Grade,Sem8_Grade,Fathers_Name,Mothers_Name,Fathers_Occupation,Mothers_Occupation,Twelfth_School,Tenth_School,Twelfth_Board,Tenth_Board,Citizenship,City,State,Address,Aadhaar,Branch,Password,Objective,CGPA) VALUES (@student_id, @name, @gender,@email,@dob,@cntnum,@tenm,@twelvem,@backlogs,@sem1,@sem2,@sem3,@sem4,@sem5,@sem6,@sem7,@sem8,@fname,@mname,@fo,@mo,@twelves,@tens,@twelveb,@tenb,@citizen,@city,@state,@addr,@aadhaar,@branch,@password,@objective,@cgpa)", con);

            //To store intermediate temporary integer and float values
            int temp_num;
            float temp_flo;

            int.TryParse(RegNoTextBox.Text.ToString(), out temp_num);
            cmd.Parameters.AddWithValue("@student_id", temp_num);

            cmd.Parameters.AddWithValue("@name", NameTextBox.Text);
            cmd.Parameters.AddWithValue("@gender", GenderTextBox.Text);
            cmd.Parameters.AddWithValue("@email", EmailTextBox.Text);
            cmd.Parameters.AddWithValue("@dob", DOBTextBox.Text);
            cmd.Parameters.AddWithValue("@cntnum", PhoneTextBox.Text);
            cmd.Parameters.AddWithValue("@tenm", XthStandardTextBox.Text);
            cmd.Parameters.AddWithValue("@twelvem", XIIthStandardTextBox.Text);
            cmd.Parameters.AddWithValue("@backlogs", BacklogsTextBox.Text);

            float.TryParse(SemTextBox1.Text.ToString(), out temp_flo);
            cmd.Parameters.AddWithValue("@sem1", temp_flo);

            float.TryParse(SemTextBox2.Text.ToString(), out temp_flo);
            cmd.Parameters.AddWithValue("@sem2", temp_flo);

            float.TryParse(SemTextBox3.Text.ToString(), out temp_flo);
            cmd.Parameters.AddWithValue("@sem3", temp_flo);

            float.TryParse(SemTextBox4.Text.ToString(), out temp_flo);
            cmd.Parameters.AddWithValue("@sem4", temp_flo);

            float.TryParse(SemTextBox5.Text.ToString(), out temp_flo);
            cmd.Parameters.AddWithValue("@sem5", temp_flo);

            float.TryParse(SemTextBox6.Text.ToString(), out temp_flo);
            cmd.Parameters.AddWithValue("@sem6", temp_flo);

            float.TryParse(SemTextBox7.Text.ToString(), out temp_flo);
            cmd.Parameters.AddWithValue("@sem7", temp_flo);

            float.TryParse(SemTextBox8.Text.ToString(), out temp_flo);
            cmd.Parameters.AddWithValue("@sem8", temp_flo);

            cmd.Parameters.AddWithValue("@fname", FatherNameTextBox.Text);
            cmd.Parameters.AddWithValue("@mname", MotherNameTextBox.Text);
            cmd.Parameters.AddWithValue("@fo", FatherOccupationTextBox.Text);
            cmd.Parameters.AddWithValue("@mo", MotherOccupationTextBox.Text);
            cmd.Parameters.AddWithValue("@twelves", XIISchoolTextBox.Text);
            cmd.Parameters.AddWithValue("@tens", XSchoolTextBox.Text);
            cmd.Parameters.AddWithValue("@twelveb", XIIBoardTextBox.Text);
            cmd.Parameters.AddWithValue("@tenb", XBoardTextBox.Text);
            cmd.Parameters.AddWithValue("@citizen", CitizenTextBox.Text);
            cmd.Parameters.AddWithValue("@city", CityTextBox.Text);
            cmd.Parameters.AddWithValue("@state", StateTextBox.Text);
            cmd.Parameters.AddWithValue("@addr", AddressTextBox.Text);
            cmd.Parameters.AddWithValue("@aadhaar", AadhaarTextBox.Text);
            cmd.Parameters.AddWithValue("@branch", BranchTextBox.Text);
            cmd.Parameters.AddWithValue("@password", PasswordTextBox.Text);
            cmd.Parameters.AddWithValue("@objective", "Fill On priority");
            cmd.Parameters.AddWithValue("@cgpa", 0);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                Label1.Text = "Successful";
            }
            catch (Exception err)
            {
                Label1.Text = err.ToString();
            }
            finally
            {
                con.Close();
            }

        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("Index.aspx");
        }
    }
}
