﻿<%@ Page Title="Student Main" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="StudentMain.aspx.cs" Inherits="Placement_Portal.StudentMain" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <form id="student_main_form" runat = "server" style="height: 394px" >
    <div id =" sidebar">
            <ul class =" nav nav-bar" style="list-style-type:none">
                <li><a runat ="server" href="EditResume.aspx">Edit Resume</a></li><br /><br />
                <li style="width: 217px"><a runat ="server" href="View_Details.aspx" >View Details</a></li><br /><br />
                <li style="width: 221px"><a runat ="server" href="CreateStudentProfile.aspx">Create Student Profile</a></li><br /><br />
                <li style="width: 177px"><a runat ="server" href="ViewStudentProfile.aspx">View Student Profiles</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </li><br /><br />
                <li style="width: 234px"><a runat ="server" href="JobReg.aspx">Register for Jobs </a></li><br /><br />
                <li style="width: 238px"><a runat ="server" href="ViewRegisteredJobs.aspx">View Registered Companies</a></li><br /><br />
                <li style="width: 239px"><a runat ="server" href="ViewSchedule.aspx">View Schedule </a></li><br /><br />
                <li style="width: 240px"><a runat ="server" href="ViewGroups.aspx">View Student Groups</a></li><br /><br />
                <li style="width: 240px"><a runat ="server" href="ContactDetails.aspx">Contact Details</a></li><br /><br />
                <asp:Button runat ="server" ID="groupAdminBtn" Visible ="false" OnClick="groupAdminBtn_Click" Text="GroupAdmin Mode" />
            </ul>
        </div>
       <asp:Label ID="Label1" runat="server"></asp:Label>
       <div id ="announce" runat="server" style="position:center; margin-left: 381px; width: 564px; margin-top: 0px;">
           &nbsp;&nbsp;&nbsp;&nbsp;
       <asp:GridView ID="GridView1" runat="server" Height="162px" style="margin-left: 54px; margin-top: 0px" Width="428px"></asp:GridView>
       </div>
       <br />
        <br />
        <asp:Button ID="BackButton" runat="server" Text="Back" OnClick="BackButton_OnClick" />
        <br />
       </form>
    
</asp:Content>
