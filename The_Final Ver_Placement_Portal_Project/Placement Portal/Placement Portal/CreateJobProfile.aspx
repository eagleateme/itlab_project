﻿<%@ Page Title="Add Job Profiles" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="CreateJobProfile.aspx.cs" Inherits="Placement_Portal.CreateJobProfile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <style>
        .wide{
            
            border:1px solid #456879;
	        border-radius:10px;
            width : 40%;
        }
        </style>

    <form id="form1" runat="server" style="height: 715px">
        <br />
        <br />
        <br />
        <asp:Label ID="JobIDLabel" runat="server" Text="Job ID : "></asp:Label>
        <br />
        <asp:TextBox ID="JobIDTextBox" runat="server" CssClass="wide"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="JobDescriptionLabel" runat="server" Text="Job Description : "></asp:Label>
        <br />
        <asp:TextBox ID="JobDescriptionTextBox" runat="server" CssClass = "wide"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="OfferTypeLabel" runat="server" Text="Offer Type: "></asp:Label>
        <br />
        <asp:TextBox ID="OfferTypeTextBox" runat="server" CssClass ="wide"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="ProfileTypeLabel" runat="server" Text="Profile Type :"></asp:Label>
        <br />
        <asp:TextBox ID="ProfileTypeTextBox" runat="server" CssClass ="wide"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="CTCLabel" runat="server" Text="CTC"></asp:Label>
        <br />
        <asp:TextBox ID="CTCTextBox" runat="server" CssClass ="wide"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="StipendLabel" runat="server" Text="Stipend"></asp:Label>
        <br />
        <asp:TextBox ID="StipendTextBox" runat="server" CssClass ="wide"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="CompanyIDLabel" runat="server" Text="Company_ID"></asp:Label>
        <br />
        <asp:TextBox ID="CompanyIDTextBox" runat="server" CssClass ="wide"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="EligibleBranchLabel" runat="server" Text="Eligible Branch"></asp:Label>
        <br />
        <asp:TextBox ID="EligibleBranchTextBox" runat="server" CssClass ="wide"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="MinCGPALabel" runat="server" Text="Min_CGPA"></asp:Label>
        <br />
        <asp:TextBox ID="MinCGPATextBox" runat="server" CssClass ="wide"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="MaxBacklogsLabel" runat="server" Text="Max_Backlogs"></asp:Label>
        <br />
        <asp:TextBox ID="MaxBacklogsTextBox" runat="server" CssClass ="wide"></asp:TextBox>
        <br />
        <br />
        <br />
        <br />
        <asp:Button ID="CreateJobProfileButton" runat="server" Height="32px" Text="Create Job Profile" Width="164px" OnClick="CreateJobProfileButton_OnClick" />
        <br />
        <br />
        <asp:Label ID="Label1" runat="server"></asp:Label>
        <br />
        <br />
        <asp:Button ID="BackButton" runat="server" Text="Back" OnClick="BackButton_OnClick" />
        <br />

    </form>
</asp:Content>
