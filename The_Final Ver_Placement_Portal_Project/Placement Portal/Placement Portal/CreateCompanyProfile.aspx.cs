﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.DynamicData;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.SqlTypes;


namespace Placement_Portal
{
    public partial class CreateCompanyProfile : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            Page.Theme = (string)Session["Theme"];
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void CreateCompanyProfileButton_OnClick(object sender,EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source = (localdb)\MSSQLlocalDB; Initial Catalog = Placement_Database; Integrated Security = True";
            SqlCommand cmd = new SqlCommand("INSERT into Company(Company_ID, Name, Website) VALUES (@compid, @compname, @compweb)", con);

            int temp_num;
            int.TryParse(CompanyIDTextBox.Text.ToString(), out temp_num);
            cmd.Parameters.AddWithValue("@compid", temp_num);
            cmd.Parameters.AddWithValue("@compname",CompanyNameTextBox.Text.ToString());
            cmd.Parameters.AddWithValue("@compweb", CompanyWebsiteTextBox.Text.ToString());           

            try
            {
                con.Open();
                //Insert into Companies Table
                cmd.ExecuteNonQuery();

                cmd.CommandText = "INSERT into Recruiter(Recruiter_ID, Company_ID, Password) VALUES (@compid, @compid, @pwd)";
                cmd.Parameters.AddWithValue("@pwd", PasswordTextBox.Text);
                cmd.ExecuteNonQuery();
                Label1.Text = "Profile Created Successfully!!";

            }
            catch (Exception err)
            {
                Label1.Text = err.ToString();
            }
            finally
            {
                con.Close();
            }
        }

        protected void BackButton_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("AdminMain.aspx");
        }
    }
}