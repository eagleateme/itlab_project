﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Placement_Portal
{
    public partial class SearchStudentProfiles : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            Page.Theme = (string)Session["Theme"];
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string url = "SearchStudentProfilesResults.aspx?";
            url += "Student_ID=" + Server.UrlEncode(TextBox1.Text);
            Response.Redirect(url);
        }

        protected void BackButton_OnClick(object sender, EventArgs e)
        {
            if (Session["recruiter_ID"] == null)
                Response.Redirect("AdminMain.aspx");
            else
                Response.Redirect("RecruiterMain.aspx");
        }
    }
}