﻿<%@ Page Title="Create Student Profile" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="CreateStudentProfile.aspx.cs" Inherits="Placement_Portal.CreateStudentProfile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        .wider{
            
            border:1px solid #456879;
	        border-radius:10px;
            width : 40%;
           
        }
         .wide{
            
            border:1px solid #456879;
	        border-radius:10px;
            width : 100%;
        }
      
     #CreateStudentProfileForm {
         height: 463px;
     }      

    </style>
    <form id="CreateStudentProfileForm" runat="server">

    <asp:Label ID="ProfileTypeLabel" Text="Profile Type" runat="server"></asp:Label>
    <br />
    <asp:TextBox ID="ProfileTypeTextBox" runat="server" CssClass="wide"></asp:TextBox>
    <br />
    <br />
    <br />
    <br />
    <asp:Label ID="SkillsLabel" Text="Skills ( Related ) " runat="server"></asp:Label>
    <br />
    <asp:TextBox ID="SkillsTextBox" runat="server" CssClass="wide"></asp:TextBox>
    <br />
    <br />
    <br />
        <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
    <br />
    <asp:Label ID="AchievementsLabel" Text="Achievements" runat="server"></asp:Label>
    <br />
    <asp:TextBox ID="AchievementsTextBox" runat="server" CssClass="wide"></asp:TextBox>
    <br />
    <br />
    <br />
    <br />
    <asp:Label ID="RelatedProjectsLabel" Text="Related Projects (Enter the titles seperated by commas) " runat="server"></asp:Label>
    <br />
    <asp:TextBox ID="RelatedProjectsTextBox" runat="server" CssClass="wide"></asp:TextBox>
    <br />
        
    &nbsp;
        <br />
        <br />
        <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="CreateStudentProfileButton" runat="server" OnClick= "CreateStudentProfile_OnClick" Text="Create a Profile" />
        <br />
        <asp:Label ID="Label2" runat="server"></asp:Label>
        <br />
        <asp:Button ID="BackButton" runat="server" Text="Back" OnClick="BackButton_OnClick" />
        <br />
    </form>

</asp:Content>
