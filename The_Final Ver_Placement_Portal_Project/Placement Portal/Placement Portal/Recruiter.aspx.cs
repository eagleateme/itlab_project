﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace Placement_Portal
{
    public partial class Recruiter : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            Page.Theme = (string)Session["Theme"];
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Session["username"] = null;
            if (!IsPostBack)
            {

            }
        }

        protected void LoginButton_Click(object sender, EventArgs e)
        {
            int curr_id;

            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source = (localdb)\MSSQLlocalDB; Initial Catalog = Placement_Database ; Integrated Security = True ;";
            try
            {
                con.Open();

                //Command to update single-valued entries
                SqlCommand cmd = new SqlCommand("SELECT Password FROM Recruiter WHERE Recruiter_ID = @recruiter_id", con);

                int.TryParse(UsernameTextBox.Text.ToString(), out curr_id);
                cmd.Parameters.AddWithValue("@recruiter_id", curr_id);
                SqlDataReader reader = cmd.ExecuteReader();
                reader.Read();

                if (PasswordTextBox.Text.Equals(reader["Password"].ToString()))
                {
                    int temp_id;
                    int.TryParse(UsernameTextBox.Text.ToString(), out temp_id);
                    Session["recruiter_ID"] = temp_id;
                    Response.Redirect("RecruiterMain.aspx");
                }
                else
                {
                    ErrorLabel.Text += "Invalid Login Credentials";
                }
            }
            catch (Exception err)
            {
                ErrorLabel.Text = err.ToString();
            }
            finally
            {
                con.Close();
            }
        }
        protected void BackButton_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("Index.aspx");
        }
    }
}