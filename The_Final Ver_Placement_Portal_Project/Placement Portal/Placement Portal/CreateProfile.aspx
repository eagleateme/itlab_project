﻿<%@ Page Title="Create Profile" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="CreateProfile.aspx.cs" Inherits="Placement_Portal.CreateProfile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <style>
        .txtboxstyle{
            border:1px solid #456879;
	        border-radius:10px;           
        }
         #form1 {

             width: 1051px;
             margin-right: 0px;
             height: 1058px;
         }
    </style>

    <form id="form1" runat="server">
        <div id = "Personal_Details" runat="server">
            <asp:Label ID="PersonalDetails_Label" Text="Personal Details" runat="server" Font-Bold="True" Font-Overline="False" Font-Underline="True"></asp:Label>
            <br />
            <asp:RequiredFieldValidator  ID="RequiredFieldValidator1" runat="server" ControlToValidate="RegNoTextBox" ErrorMessage="RegNo. Cannot be Empty" ViewStateMode="Enabled" ValidationGroup= "1"></asp:RequiredFieldValidator>
&nbsp;&nbsp;&nbsp;
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="NameTextBox" ErrorMessage="Name Cannot be Empty" ValidationGroup= "1"></asp:RequiredFieldValidator>
&nbsp;&nbsp;&nbsp;
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="DOBTextBox" ErrorMessage="DOB cannot be empty" ValidationGroup= "1"></asp:RequiredFieldValidator>
&nbsp;&nbsp;&nbsp;
            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="EmailTextBox" ErrorMessage="Email Cannot be Empty" ValidationGroup= "1"></asp:RequiredFieldValidator>--%>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="PhoneTextBox" ErrorMessage="Phone Number Cannot be Empty" ValidationGroup= "1"></asp:RequiredFieldValidator>
            <br />
             <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="RegNoTextBox" ErrorMessage="The RegNo should be in the range of 100-65535" MaximumValue="65535" MinimumValue="100" Type="Integer" ValidationGroup= "1"></asp:RangeValidator>
&nbsp;&nbsp;&nbsp;
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="EmailTextBox" ErrorMessage="Valid Email Please!!" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup= "1"></asp:RegularExpressionValidator>
&nbsp;&nbsp;&nbsp;
             <asp:Table ID="Personal_Details_Table" runat="server" CellSpacing = "1" Height="135px" Width="906px">
                 <asp:TableRow><asp:TableHeaderCell Text =" Reg.No : "> </asp:TableHeaderCell><asp:TableCell><asp:TextBox  ID="RegNoTextBox" runat="server" CssClass="txtboxstyle"></asp:TextBox>
                 </asp:TableCell><asp:TableHeaderCell Text =" Name : "> </asp:TableHeaderCell><asp:TableCell><asp:TextBox  ID="NameTextBox" runat="server" CssClass="txtboxstyle"></asp:TextBox></asp:TableCell></asp:TableRow>
                 <asp:TableRow><asp:TableHeaderCell Text =" Gender : "> </asp:TableHeaderCell><asp:TableCell><asp:TextBox  ID="GenderTextBox" runat="server" CssClass="txtboxstyle"></asp:TextBox></asp:TableCell><asp:TableHeaderCell Text =" DOB : "> </asp:TableHeaderCell><asp:TableCell><asp:TextBox  ID="DOBTextBox" runat="server"  CssClass="txtboxstyle"></asp:TextBox></asp:TableCell></asp:TableRow>
                 <asp:TableRow><asp:TableHeaderCell Text =" Email  : "> </asp:TableHeaderCell><asp:TableCell><asp:TextBox  ID="EmailTextBox" runat="server" CssClass="txtboxstyle"></asp:TextBox></asp:TableCell><asp:TableHeaderCell Text ="Ph.No : "> </asp:TableHeaderCell><asp:TableCell><asp:TextBox  ID="PhoneTextBox" runat="server" CssClass="txtboxstyle"></asp:TextBox></asp:TableCell></asp:TableRow>
            </asp:Table>
       </div>
        <br />
        <div id = "Academic Details">
            <asp:Label ID="Academic_Details_Label" Text="Academic Details" runat="server" Font-Bold="True" Font-Overline="False" Font-Underline="True"></asp:Label>
            <br />
            <asp:Table ID="Academic_Details_Table1" runat="server" CellSpacing="1" Height="140px" Width="851px">
            <asp:TableRow><asp:TableHeaderCell Text="Xth Standard" Width= "150px"></asp:TableHeaderCell><asp:TableHeaderCell  Width= "150px" Text="XIIth Standard"></asp:TableHeaderCell><asp:TableHeaderCell  Width= "150px" Text="Backlogs"></asp:TableHeaderCell></asp:TableRow>
            <asp:TableRow><asp:TableCell><asp:TextBox   Width= "150px" ID="XthStandardTextBox" runat="server" CssClass="txtboxstyle"></asp:TextBox></asp:TableCell><asp:TableCell><asp:TextBox   Width= "150px" ID="XIIthStandardTextBox" runat="server" CssClass="txtboxstyle"></asp:TextBox></asp:TableCell><asp:TableCell><asp:TextBox   Width= "150px" ID="BacklogsTextBox" runat="server" CssClass="txtboxstyle"></asp:TextBox></asp:TableCell></asp:TableRow>
            </asp:Table>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ValidationGroup= "1" ControlToValidate="XthStandardTextBox" ErrorMessage="Xth Standard a must!"></asp:RequiredFieldValidator>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ValidationGroup= "1" ControlToValidate="XIIthStandardTextBox" ErrorMessage="XIIth Standard a must!"></asp:RequiredFieldValidator>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ValidationGroup= "1" ControlToValidate="BacklogsTextBox" ErrorMessage="Details on Backlogs !!!"></asp:RequiredFieldValidator>
            <br />
            <br />
             <asp:Table ID="Academic_Details_Table2" runat="server" CellSpacing="1" Height="127px" Width="851px">
              <asp:TableRow><asp:TableHeaderCell Text="Semester" Width="100px"></asp:TableHeaderCell><asp:TableHeaderCell Text="Sem1" Width="50px"></asp:TableHeaderCell><asp:TableHeaderCell Text="Sem2" Width="50px"></asp:TableHeaderCell><asp:TableHeaderCell Text="Sem3" Width="50px"></asp:TableHeaderCell><asp:TableHeaderCell Text="Sem4" Width="50px"></asp:TableHeaderCell><asp:TableHeaderCell Text="Sem5" Width="50px"></asp:TableHeaderCell><asp:TableHeaderCell Text="Sem6" Width="50px"></asp:TableHeaderCell><asp:TableHeaderCell Text="Sem7" Width="50px"></asp:TableHeaderCell><asp:TableHeaderCell Text="Sem8" Width="50px"></asp:TableHeaderCell></asp:TableRow>
              <asp:TableRow><asp:TableHeaderCell Text="GPA     " Width="100px"></asp:TableHeaderCell><asp:TableCell><asp:TextBox   Width= "50px" ID="SemTextBox1" runat="server" CssClass="txtboxstyle" ></asp:TextBox></asp:TableCell><asp:TableCell><asp:TextBox   Width= "50px" ID="SemTextBox2"  runat="server" CssClass="txtboxstyle"></asp:TextBox></asp:TableCell><asp:TableCell><asp:TextBox   Width= "50px" ID="SemTextBox3"  runat="server" CssClass="txtboxstyle"></asp:TextBox></asp:TableCell><asp:TableCell><asp:TextBox   Width= "50px" ID="SemTextBox4"  runat="server" CssClass="txtboxstyle"></asp:TextBox></asp:TableCell><asp:TableCell><asp:TextBox   Width= "50px" ID="SemTextBox5" runat="server"   CssClass="txtboxstyle"></asp:TextBox></asp:TableCell><asp:TableCell><asp:TextBox   Width= "50px" ID="SemTextBox6"  runat="server" CssClass="txtboxstyle"></asp:TextBox></asp:TableCell><asp:TableCell><asp:TextBox   Width= "50px" ID="SemTextBox7" runat="server" CssClass="txtboxstyle"></asp:TextBox></asp:TableCell><asp:TableCell><asp:TextBox   Width= "50px" ID="SemTextBox8" runat="server" CssClass="txtboxstyle"></asp:TextBox></asp:TableCell></asp:TableRow>  
             </asp:Table>
            </div>
        <br />
        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ValidationGroup= "1" runat="server" ControlToValidate="SemTextBox1" ErrorMessage="Sem I grade  !" ></asp:RequiredFieldValidator>
        &nbsp;&nbsp;&nbsp;
        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator10" ValidationGroup= "1" runat="server" ControlToValidate="SemTextBox2" ErrorMessage="Sem II grades  !" ></asp:RequiredFieldValidator>--%>
        &nbsp;&nbsp;&nbsp;
        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ValidationGroup= "1" runat="server" ControlToValidate="SemTextBox3" ErrorMessage="Sem III grades !" ></asp:RequiredFieldValidator>
        &nbsp;&nbsp;&nbsp;
        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ValidationGroup= "1" runat="server" ControlToValidate="SemTextBox4" ErrorMessage="Sem IV grades !" ></asp:RequiredFieldValidator>
        &nbsp;&nbsp;&nbsp;
        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" ValidationGroup= "1" runat="server" ControlToValidate="SemTextBox5" ErrorMessage="Sem V grades  !" ></asp:RequiredFieldValidator>
&nbsp;&nbsp;&nbsp;
        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ValidationGroup= "1" runat="server" ControlToValidate="SemTextBox6" ErrorMessage="Sem VI grades  !" ></asp:RequiredFieldValidator>
        <br />
        <br />
       <div id = "Extra Details">
            <asp:Label ID="Extra_Details_Label" Text="Extra Details" runat="server" Font-Bold="True" Font-Overline="False" Font-Underline="True"></asp:Label>
            <br />
            <br />
            <br />
            <asp:Table ID="Extra_Details_Table" runat="server" CellSpacing="1" Width="864px" Height="290px">
                <asp:TableHeaderRow><asp:TableHeaderCell Text="Mother's Name"></asp:TableHeaderCell><asp:TableCell><asp:TextBox  ID="MotherNameTextBox" runat="server"></asp:TextBox></asp:TableCell><asp:TableHeaderCell Text="Father's Name"></asp:TableHeaderCell><asp:TableCell><asp:TextBox  ID="FatherNameTextBox" runat="server"></asp:TextBox></asp:TableCell></asp:TableHeaderRow>
                <asp:TableHeaderRow><asp:TableHeaderCell Text="Mother's Occupation"></asp:TableHeaderCell><asp:TableCell><asp:TextBox  ID="MotherOccupationTextBox" runat="server"></asp:TextBox></asp:TableCell><asp:TableHeaderCell Text="Father's Occupation"></asp:TableHeaderCell><asp:TableCell><asp:TextBox  ID="FatherOccupationTextBox" runat="server"></asp:TextBox></asp:TableCell></asp:TableHeaderRow>
                <asp:TableHeaderRow><asp:TableHeaderCell Text="Xth School"></asp:TableHeaderCell><asp:TableCell><asp:TextBox  ID="XSchoolTextBox" runat="server"></asp:TextBox></asp:TableCell><asp:TableHeaderCell Text="Xth Board"></asp:TableHeaderCell><asp:TableCell><asp:TextBox  ID="XBoardTextBox" runat="server"></asp:TextBox></asp:TableCell></asp:TableHeaderRow>
                <asp:TableHeaderRow><asp:TableHeaderCell Text="XIIth School"></asp:TableHeaderCell><asp:TableCell><asp:TextBox  ID="XIISchoolTextBox" runat="server"></asp:TextBox></asp:TableCell><asp:TableHeaderCell Text="XIIth Board"></asp:TableHeaderCell><asp:TableCell><asp:TextBox  ID="XIIBoardTextBox" runat="server"></asp:TextBox></asp:TableCell></asp:TableHeaderRow>
                <asp:TableHeaderRow><asp:TableHeaderCell Text="Citizenship"></asp:TableHeaderCell><asp:TableCell><asp:TextBox  ID="CitizenTextBox" runat="server"></asp:TextBox></asp:TableCell><asp:TableHeaderCell Text="State"></asp:TableHeaderCell><asp:TableCell><asp:TextBox  ID="StateTextBox" runat="server"></asp:TextBox></asp:TableCell></asp:TableHeaderRow>
                <asp:TableHeaderRow><asp:TableHeaderCell Text="City"></asp:TableHeaderCell><asp:TableCell><asp:TextBox  ID="CityTextBox" runat="server"></asp:TextBox></asp:TableCell><asp:TableHeaderCell Text="Address"></asp:TableHeaderCell><asp:TableCell><asp:TextBox  ID="AddressTextBox" runat="server"></asp:TextBox></asp:TableCell></asp:TableHeaderRow>
                <asp:TableHeaderRow><asp:TableHeaderCell Text="Aadhaar"></asp:TableHeaderCell><asp:TableCell><asp:TextBox  ID="AadhaarTextBox" runat="server"></asp:TextBox></asp:TableCell><asp:TableHeaderCell Text="Password"></asp:TableHeaderCell><asp:TableCell><asp:TextBox  ID="PasswordTextBox" runat="server"></asp:TextBox></asp:TableCell></asp:TableHeaderRow>
           <asp:TableHeaderRow><asp:TableHeaderCell Text="Branch"></asp:TableHeaderCell><asp:TableCell><asp:TextBox  ID="BranchTextBox" runat="server"></asp:TextBox></asp:TableCell></asp:TableHeaderRow>
                </asp:Table>
        </div>
        <br />
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Create Profile" ValidationGroup ="1" />
        <div style="margin-left: 40px">
            <asp:Label ID="Label1" runat="server"></asp:Label>
        </div>
        <br />
        <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Back" ValidationGroup= "2"/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <br />
        <br />
        <br />
    </form>
</asp:Content>
