﻿<%@ Page Title="Group Admin Mode" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="GroupAdminMain.aspx.cs" Inherits="Placement_Portal.GroupAdminMain" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
 <form id="form1" runat="server">
        <br />
        Group ID:
        <asp:TextBox ID="GroupIDTextBox" runat="server"></asp:TextBox>
        <br />
        <br />
        Enter the student IDs of the members you want to add/delete<br />
        <br />
        <asp:TextBox ID="AddMembersTextBox" runat="server"></asp:TextBox>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="DeleteMembersTextBox" runat="server"></asp:TextBox>
        <br />
        <br />
        <asp:Button ID="AddMembersButton" runat="server" Text="Add Members"  OnClick="AddMembersButton_OnClick"/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="DeleteMembersButton" runat="server" Text="Delete Members"  OnClick="DeleteMembersButton_OnClick"/>
        <br />
        <br />
        <asp:Label ID="Label6" runat="server" Text="Please Fill All the TextBoxes other than Member TextBox to Create a Group"></asp:Label>
        <br />
        <br />
        <asp:Label ID="Label7" runat="server"></asp:Label>
        <br />
        <br />
        <asp:Label ID="Label8" runat="server" Text="Fill In the Admin and Group Id to Reassign Admin"></asp:Label>
        <br />
        <br />
        Group Admin:
        <asp:TextBox ID="GroupAdminTextBox" runat="server"></asp:TextBox>
        <br />
        <br />
        <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Re assign Admin" />
        <br />
        <br />
        <br />
        <br />
        <br />
        <asp:Button ID="BackButton" runat="server" Text="Back" OnClick="BackButton_OnClick" />
        <br />
    </form>
</asp:Content>
