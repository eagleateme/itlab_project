﻿<%@ Page Title="Edit Resume" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="EditResume.aspx.cs" Inherits="Placement_Portal.EditResume" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id ="resumeForm" runat="server">
        <asp:Label ID="Label13" runat="server"></asp:Label>
    <style>
        .wider{
            
            border:1px solid #456879;
	        border-radius:10px;
            width : 40%;
           
        }
         .wide{
            
            border:1px solid #456879;
	        border-radius:10px;
            width : 100%;
        }

    </style>
        <br />
        <div>
        <asp:Label ID="Label1" runat="server" Text="Objective:" ForeColor ="#660000"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxObjective" ErrorMessage="Objective Field Cannot be Empty" ForeColor="#0066FF"></asp:RequiredFieldValidator>
        <br/>
        <asp:TextBox  ID="TextBoxObjective" runat="server" CssClass="wider" Height="16px" ></asp:TextBox>
            <br />
        <br />
        <asp:Label ID="Label2" runat="server" Text="Areas of Interest:" ForeColor ="#660000"></asp:Label>
        <br />
        <asp:TextBox  ID="TextBoxAreasOfI" runat="server" CssClass="wider"></asp:TextBox>
            <br />
        <br />
        <asp:Label ID="Label3" runat="server" Text="Languages:" ForeColor ="#660000"></asp:Label>
        <br />
        <asp:TextBox  ID="TextBoxLang" runat="server" CssClass="wider"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="Label4" runat="server" Text="Programming Languages and Software Skills: " ForeColor ="#660000"></asp:Label>
        <br />
        <asp:TextBox  ID="TextBoxSkills" runat="server" CssClass="wider"></asp:TextBox>
            <br />
        <br />
        <asp:Label ID="Label6" runat="server" Text="Online Courses: " ForeColor ="#660000"></asp:Label>
        <br />
        <asp:TextBox  ID="TextBoxOnlineCourse" runat="server" CssClass="wider"></asp:TextBox>
            <br />
        <br />
        <asp:Label ID="Label7" runat="server" Text="Electives: " ForeColor ="#660000"></asp:Label>
        <br />
        <asp:TextBox  ID="TextBoxElec" runat="server" CssClass="wider"></asp:TextBox>
            <br />
        <br />
        <asp:Label ID="Label8" runat="server" Text="Extra Curricular Activities: " ForeColor ="#660000"></asp:Label>
        <br />
        <asp:TextBox  ID="TextBoxExtraCAct" runat="server" CssClass="wider"></asp:TextBox>
            <br />
        <br />
        <asp:Label ID="Label9" runat="server" Text="Social Volunteering: " ForeColor ="#660000"></asp:Label>
        <br />
        <asp:TextBox  ID="TextBoxSocVol" runat="server" CssClass="wider"></asp:TextBox>
            <br />
        <br />
        <asp:Label ID="Label10" runat="server" Text="Achievements: " ForeColor ="#660000"></asp:Label>
        <br />
        <asp:TextBox  ID="TextBoxAchieve" runat="server" CssClass="wider"></asp:TextBox>
        <br />
        <asp:Button ID="SubmitFormButton" runat="server" OnClick="SubmitFormButton_Click" Text="Submit  Form" Height="27px" style="margin-left: 807px; margin-top: 0px" Width="262px" />
        <br />
        <asp:Label ID="Label11" runat="server" Text="Internships: " ForeColor ="#660000"></asp:Label>
        <br />
        <asp:ScriptManager ID ="ScriptManager1" runat ="server"></asp:ScriptManager>
        <p style ="width:40%; height: 7px;">
            <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </p>
        <p style ="height:8px; width: 14px">
            <asp:Button ID="addButton1" runat="server" Text="Add an Internship Field" OnClick= "addButton1_Click" style ="margin-left: 805px" Width="264px" />
        </p>
        <br />
        <asp:Label ID="Label12" runat="server" Text="Projects: " ForeColor ="#660000"></asp:Label>
        <br />
        <p style ="width:40%; height: 43px;">
            <asp:PlaceHolder ID="PlaceHolder2" runat="server"></asp:PlaceHolder>
        </p>
        <p style ="height:8px; width: 14px">
            <asp:Button ID="Button2" runat="server" Text="Add a Project Field" OnClick= "addButton2_Click" style ="margin-left: 805px; margin-bottom: 95px;" Width="264px" />
        </p>
            </div>
        <br />
        <br />
        <asp:Button ID="BackButton" runat="server" Text="Back" OnClick="BackButton_OnClick" />
        <br />
    </form>
</asp:Content>
