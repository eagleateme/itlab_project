﻿<%@ Page Title="Recruiter Home" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="RecruiterMain.aspx.cs" Inherits="Placement_Portal.RecruiterMain" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="recruiter_main_form" runat = "server" >   
    <div id =" sidebar">
            <ul class =" nav nav-bar" style="list-style-type:none">
                <li><a runat ="server" href="ViewGroups.aspx">View Student Groups</a></li><br /><br />
                <li><a runat ="server" href="SearchStudents.aspx">Search Students</a></li><br /><br />
                <li><a runat ="server" href="SearchStudentProfiles.aspx">View Profiles Based On Student ID</a></li><br /><br />
                <li><a runat ="server" href="ViewRegStudents.aspx">View Registered Students</a></li><br /><br />
                <li><a runat ="server" href="ViewSchedule.aspx">View Schedule</a></li><br /><br />
            </ul>
        </div>
        <br />
        <br />
        <asp:Button ID="BackButton" runat="server" Text="Back" OnClick="BackButton_OnClick" />
        <br />
       </form>
</asp:Content>
