﻿<%@ Page Title="Admin Main" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="AdminMain.aspx.cs" Inherits="Placement_Portal.AdminMain" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <form id="admin_main_form" runat = "server" >   
    <div id =" sidebar">
            <ul class =" nav nav-bar" style="list-style-type:none">
                <li><a runat ="server" href="EventSchedule.aspx">Event Scheduling</a></li><br /><br />
                <li><a runat ="server" href="CreateJobProfile.aspx">Create Job Profiles</a></li><br /><br />
                <li><a runat ="server" href="CreateCompanyProfile.aspx">Create Company Profiles</a></li><br /><br />
                <li><a runat ="server" href="CreateStudentGroups.aspx"> Create, Delete Or Modify Student Groups</a></li><br /><br />
                <li><a runat ="server" href="ViewGroups.aspx">View Student Groups</a></li><br /><br />
                <li><a runat ="server" href="SearchStudents.aspx">Search Students</a></li><br /><br />
                <li><a runat ="server" href="SearchStudentProfiles.aspx">View Profiles Based On Student ID</a></li><br /><br />
                <li><a runat ="server" href="AnnouncementBroadcast.aspx">Broadcast Announcement</a></li><br /><br />
                <li><a runat ="server" href="DeleteStudentAccount.aspx">Delete Student Accounts</a></li><br /><br />
            </ul>
        </div>
        <br />
        <br />
        <asp:Button ID="BackButton" runat="server" Text="Back" OnClick="BackButton_OnClick" />
        <br />
       </form>
</asp:Content>