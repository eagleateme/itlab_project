﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.DynamicData;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace Placement_Portal
{
    public partial class CreateJobProfile : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            Page.Theme = (string)Session["Theme"];
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void CreateJobProfileButton_OnClick(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source = (localdb)\MSSQLlocalDB; Initial Catalog = Placement_Database; Integrated Security = True";
            SqlCommand cmd = new SqlCommand("INSERT into Job_Profile(Job_ID, Job_Description, Offer_Type,Profile_Type,CTC,Stipend,Company_ID,Eligible_Branch,Min_CGPA,Max_Backlogs) VALUES (@jobid,@job_desc,@offer,@profile,@ctc,@stipend,@compid,@eligible,@mincgpa,@maxback)", con);

            int temp_num;
            int.TryParse(JobIDTextBox.Text.ToString(), out temp_num);
            cmd.Parameters.AddWithValue("@jobid", temp_num);
            cmd.Parameters.AddWithValue("@job_desc", JobDescriptionTextBox.Text.ToString());
            cmd.Parameters.AddWithValue("@offer", OfferTypeTextBox.Text.ToString());
            cmd.Parameters.AddWithValue("@profile", ProfileTypeTextBox.Text.ToString());
            cmd.Parameters.AddWithValue("@ctc", CTCTextBox.Text.ToString());
            cmd.Parameters.AddWithValue("@stipend", StipendTextBox.Text.ToString());

            int.TryParse(CompanyIDTextBox.Text.ToString(), out temp_num);
            cmd.Parameters.AddWithValue("@compid", temp_num);

            cmd.Parameters.AddWithValue("@eligible", EligibleBranchTextBox.Text.ToString());
            cmd.Parameters.AddWithValue("@mincgpa", MinCGPATextBox.Text.ToString());
            cmd.Parameters.AddWithValue("@maxback", MaxBacklogsTextBox.Text.ToString());
                       

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                Label1.Text = "Profile Created Successfully!!";

            }
            catch (Exception err)
            {
                Label1.Text = err.ToString();
            }
            finally
            {
                con.Close();
            }
        }

        protected void BackButton_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("AdminMain.aspx");
        }
    }
}