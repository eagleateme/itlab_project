﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.DynamicData;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.SqlTypes;


namespace Placement_Portal
{
    public partial class Student : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            Page.Theme = (string)Session["Theme"];
        }

        HttpCookie studentLoginCookie;
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["recruiter_ID"] = null;
            if (!IsPostBack)
            {                 
                //Retreive Login cookie
                studentLoginCookie = Request.Cookies.Get("Login Credentials");

                //Load username and password if it cookie exists
                if (studentLoginCookie != null)                
                { 
                    studentLoginCookie = Request.Cookies["Login Credentials"];
                    UsernameTextBox.Text = studentLoginCookie["Username"];
                    UsernameTextBox.BackColor = System.Drawing.Color.Beige;
                    PasswordTextBox.BackColor = System.Drawing.Color.Beige;
                    PasswordTextBox.Attributes.Add("value", studentLoginCookie["Password"]);

                }
            }
    
        }

        protected void LoginButton_Click(object sender, EventArgs e)
        {
            if(RememberCheckBox.Checked == true)
            {
                //Create cookie and add it to response
                studentLoginCookie = new HttpCookie("Login Credentials");
                studentLoginCookie["Username"] = UsernameTextBox.Text;
                studentLoginCookie["Password"] = PasswordTextBox.Text;
                studentLoginCookie.Expires = DateTime.Now.AddDays(7);
                Response.Cookies.Add(studentLoginCookie);
               
            }

            int curr_id;

            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source = (localdb)\MSSQLlocalDB; Initial Catalog = Placement_Database ; Integrated Security = True ;";
            try
            {
                con.Open();

                //Command to update single-valued entries
                SqlCommand cmd = new SqlCommand("SELECT Password FROM Student_Details WHERE Student_ID = @student_id", con);

                int.TryParse(UsernameTextBox.Text.ToString(), out curr_id);
                cmd.Parameters.AddWithValue("@student_id", curr_id);
                SqlDataReader reader = cmd.ExecuteReader();
                reader.Read();

                if (PasswordTextBox.Text.Equals(reader["Password"].ToString()))
                {
                    int temp_id;
                    int.TryParse(UsernameTextBox.Text.ToString(), out temp_id);
                    Session["username"] = temp_id;

                    //Check if user is a Group Admin
                    cmd.CommandText = "SELECT * from Groups where Group_Admin = @student_id";
                    reader.Close();
                    reader = cmd.ExecuteReader();
                    Session["isGroupAdmin"] = reader.HasRows;
                    Response.Redirect("StudentMain.aspx");
                }
                else
                {
                    ErrorLabel.Text += "Invalid Login Credentials";
                }
            }
            catch(Exception err)
            {
                ErrorLabel.Text = err.ToString();
            }
            finally
            {
                con.Close();
            }
        }
        protected void BackButton_OnClick(object sender, EventArgs e)
        {
            Session["username"] = null;
            Response.Redirect("Index.aspx");
        }
    }
}