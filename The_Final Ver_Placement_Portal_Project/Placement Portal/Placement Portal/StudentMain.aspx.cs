﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.DynamicData;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace Placement_Portal
{
    public partial class StudentMain : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            Page.Theme = (string)Session["Theme"];
        }

        DataSet ds = new DataSet();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["isGroupAdmin"] != null)
                groupAdminBtn.Visible = (bool)Session["isGroupAdmin"];
            else
                groupAdminBtn.Visible = false;

            if (!IsPostBack)
            {
                SqlConnection con = new SqlConnection();
                con.ConnectionString = @"Data Source = (localdb)\MSSQLlocalDB; Initial Catalog = Placement_Database ; Integrated Security = True ;";
                try
                {
                    con.Open();

                    //Command to update single-valued entries
                    SqlCommand cmd = new SqlCommand("SELECT * FROM Announcements ", con);

                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);

                    adapter.Fill(ds, "Announcements");

                    GridView1.DataSource = ds;

                    GridView1.DataBind();

                }
                catch (Exception err)
                {
                    Label1.Text = err.ToString();
                }
                finally
                {
                    con.Close();
                }
            }            
        }

        protected void groupAdminBtn_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source = (localdb)\MSSQLlocalDB; Initial Catalog = Placement_Database ; Integrated Security = True ;";
            try
            {
                con.Open();

                SqlCommand cmd = new SqlCommand("SELECT Group_ID FROM Groups WHERE Group_Admin = @stud_id", con);
                cmd.Parameters.AddWithValue("@stud_id", Session["username"]);
                SqlDataReader reader = cmd.ExecuteReader();
                reader.Read();
                Session["Group_ID"] = reader["Group_ID"];
            }
            catch (Exception err)
            {
                Label1.Text = err.ToString();
            }
            finally
            {
                con.Close();
            }

            Response.Redirect("GroupAdminMain.aspx");
        }
        protected void BackButton_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("Student.aspx");
        }
    }
}