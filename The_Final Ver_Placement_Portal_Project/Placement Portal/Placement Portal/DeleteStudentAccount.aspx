﻿<%@ Page Title="Delete Student Accounts" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="DeleteStudentAccount.aspx.cs" Inherits="Placement_Portal.DeleteStudentAccount" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="form1" runat="server">
        <asp:Label ID="Label1" runat="server" Text="Enter a Student ID to Delete their account :"></asp:Label>
        <br />
        <br />
        <asp:TextBox ID="DeleteTextBox" runat="server"></asp:TextBox>
        <br />
        <br />
        <br />
        <asp:Label ID="Label2" runat="server" Text="Please Make Sure the Student is Not in any Groups before Deleting"></asp:Label>
        <br />
        <br />
        <asp:Button ID="Button1" runat="server" Text="Delete the Account" OnClick="Button1_OnClick" />
        <br />
        <br />
        <asp:Label ID="Label3" runat="server"></asp:Label>
        <br />
        <br />
        <asp:Button ID="BackButton" runat="server" Text="Back" OnClick="BackButton_OnClick" />
        <br />
    </form>
</asp:Content>
