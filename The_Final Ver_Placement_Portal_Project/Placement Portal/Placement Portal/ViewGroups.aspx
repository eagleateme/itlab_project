﻿<%@ Page Title="View Group Details" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="ViewGroups.aspx.cs" Inherits="Placement_Portal.ViewGroups" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <form id="form1" runat="server">
        <asp:Label ID="Label1" runat="server" Text="Select a Group Name : "></asp:Label>
        <br />
        <br />
        <asp:DropDownList ID="DropDownList1" runat="server">
        </asp:DropDownList>
        <br />
        <br />
        <asp:Button ID="Button1" runat="server" Text="View Group"  OnClick="Button1_OnClick"/>
        <br />
        <br />
        <asp:GridView ID="GridView1" runat="server" Width="291px">
        </asp:GridView>
         <br />
         <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="View Group Members" />
         <br />
         <br />
         <asp:Label ID="Label2" runat="server" Text="Group Members : "></asp:Label>
         <br />
         <asp:GridView ID="GridView2" runat="server">
         </asp:GridView>
         <br />
        <br />
        <br />
         <br />
        <br />
        <asp:Button ID="BackButton" runat="server" Text="Back" OnClick="BackButton_OnClick" />
        <br />
    </form>
</asp:Content>
