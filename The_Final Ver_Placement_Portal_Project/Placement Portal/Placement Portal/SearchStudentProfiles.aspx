﻿<%@ Page Title="Search Student Profiles" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="SearchStudentProfiles.aspx.cs" Inherits="Placement_Portal.SearchStudentProfiles" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <form id="form1" runat="server">
        <asp:Label ID="Label1" runat="server" Text="Enter Student ID"></asp:Label>
        <br />
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        <br />
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Search for Profiles" />
        <br />
        <asp:Label ID="ResultsLabel" runat="server"></asp:Label>
        <br />
      <br />
        <br />
        <asp:Button ID="BackButton" runat="server" Text="Back" OnClick="BackButton_OnClick" />
        <br />
    </form>
</asp:Content>
