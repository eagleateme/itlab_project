﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace Placement_Portal
{
    public partial class SearchStudentProfilesResults : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            Page.Theme = (string)Session["Theme"];
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string stud_id = Server.UrlDecode(Request["Student_ID"]);

            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source = (localdb)\MSSQLlocalDB; Initial Catalog = Placement_Database; Integrated Security = True";
            SqlCommand cmd = new SqlCommand("SELECT * FROM Student_Profile where Student_ID = @stu_id", con);
            cmd.Parameters.AddWithValue("@stu_id", stud_id);
            try
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    GridView1.DataSource = reader;
                    GridView1.DataBind();
                    while (reader.Read())
                    {

                    }
                }
                else
                    ResultsLabel.Text += "No Results found";
            }

            catch (Exception err)
            {
                ResultsLabel.Text = err.ToString();
            }
            finally
            {
                con.Close();
            }
        }
        protected void BackButton_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("SearchStudentProfiles.aspx");
        }
    }
}