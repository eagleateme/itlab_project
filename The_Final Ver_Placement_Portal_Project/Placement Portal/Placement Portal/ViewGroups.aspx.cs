﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.DynamicData;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace Placement_Portal
{
    public partial class ViewGroups : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            Page.Theme = (string)Session["Theme"];
        }

        DataSet ds = new DataSet();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SqlConnection con = new SqlConnection();
                con.ConnectionString = @"Data Source = (localdb)\MSSQLlocalDB; Initial Catalog = Placement_Database ; Integrated Security = True ;";
                try
                {
                    con.Open();

                    //Command to update single-valued entries
                    SqlCommand cmd = new SqlCommand("SELECT Group_Id,Group_Name FROM Groups", con);
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        DropDownList1.Items.Add(new ListItem("Group Id: " + reader["Group_Id"].ToString() + ", Name :" + reader["Group_Name"].ToString(), reader["Group_Id"].ToString()));
                    }


                }
                catch (Exception err)
                {

                }
                finally
                {
                    con.Close();
                }
            }
        }

        protected void Button1_OnClick(object sender, EventArgs e)
        {

            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source = (localdb)\MSSQLlocalDB; Initial Catalog = Placement_Database ; Integrated Security = True ;";
            try
            {
                con.Open();

                //Command to update single-valued entries
                SqlCommand cmd = new SqlCommand("SELECT * FROM Groups where Group_ID = @gid", con);
                cmd.Parameters.AddWithValue("@gid", DropDownList1.SelectedItem.Value);
                SqlDataReader reader1 = cmd.ExecuteReader();

                GridView1.DataSource = reader1;
                GridView1.DataBind();
                                              
            }
            catch (Exception err)
            {

            }
            finally
            {
                con.Close();
            }

        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source = (localdb)\MSSQLlocalDB; Initial Catalog = Placement_Database ; Integrated Security = True ;";
            try
            {
                SqlCommand cmd2 = new SqlCommand("SELECT Student_Id,Name FROM Student_Details where Group_ID = @gid", con);
                cmd2.Parameters.AddWithValue("@gid", DropDownList1.SelectedItem.Value);

                SqlDataAdapter adapter = new SqlDataAdapter(cmd2);

                adapter.Fill(ds, "Student_Details");
                
                GridView2.DataSource = ds;

                GridView2.DataBind();
            }
            catch (Exception err1)
            {
                Label1.Text = err1.ToString();
            }
            finally
            {
                con.Close();
            }

        }
        protected void BackButton_OnClick(object sender, EventArgs e)
        {
            if (Session["username"] != null)
                Response.Redirect("StudentMain.aspx");
            else if(Session["recruiter_ID"] != null)
                Response.Redirect("RecruiterMain.aspx");
            else
                Response.Redirect("AdminMain.aspx");
        }
    }
}