﻿<%@ Page Title="Broadcast Announcements" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="AnnouncementBroadcast.aspx.cs" Inherits="Placement_Portal.AnnouncementBroadcast" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="form1" runat="server">
        <asp:Label ID="Label1" runat="server" Text="Announcement ID"></asp:Label>
        :<br />
        <br />
        <asp:TextBox ID="AnnouncementIDTextBox" runat="server"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="Label2" runat="server" Text="Announcement Description : "></asp:Label>
        <br />
        <br />
        <asp:TextBox ID="AnnouncementTextBox" runat="server" Height="170px" TextMode="MultiLine" Width="361px"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="Label3" runat="server" Text="Announcement Date :"></asp:Label>
        <br />
        <br />
        <asp:TextBox ID="AnnounceDateTextBox" runat="server" TextMode= date ></asp:TextBox>
        <br />
        <br />
        <asp:Button ID="SendAnnounceButton" runat="server" Text="Send the Announcement !" OnClick="AnnounceClick" Width="159px" />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="Label4" runat="server"></asp:Label>
&nbsp;
        <br />
        <br />
        <asp:Button ID="BackButton" runat="server" Text="Back" OnClick="BackButton_OnClick" />
        <br />
    </form>
</asp:Content>
