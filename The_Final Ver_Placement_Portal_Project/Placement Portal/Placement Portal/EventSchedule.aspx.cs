﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.DynamicData;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace Placement_Portal
{
    public partial class EventSchedule : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            Page.Theme = (string)Session["Theme"];
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void CreateEventButton_OnClick(object sender,EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source = (localdb)\MSSQLlocalDB; Initial Catalog = Placement_Database; Integrated Security = True";
            SqlCommand cmd = new SqlCommand("INSERT into Events(Event_ID, Company_ID, Date, Time, Event_Description) VALUES (@eventid, @compid, @date,@time,@event_desc)", con);

            int temp_num;
            int.TryParse(EventIDTextBox.Text.ToString(), out temp_num);
            cmd.Parameters.AddWithValue("@eventid", temp_num);

            int.TryParse(CompanyIDTextBox.Text.ToString(), out temp_num);
            cmd.Parameters.AddWithValue("@compid", temp_num);
            cmd.Parameters.AddWithValue("@date", Calendar1.SelectedDate.ToString());
            cmd.Parameters.AddWithValue("@time", TimeTextBox.Text.ToString());
            cmd.Parameters.AddWithValue("@event_desc", EventDescriptionTextBox.Text.ToString());


            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                Label5.Text = "Successfull!!";

            }
            catch (Exception err)
            {
                Label5.Text = err.ToString();
            }
            finally
            {
                con.Close();
            }
        }

        protected void BackButton_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("AdminMain.aspx");
        }
    }
}