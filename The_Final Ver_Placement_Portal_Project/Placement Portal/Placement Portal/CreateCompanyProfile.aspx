﻿<%@ Page Title="Add Company" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="CreateCompanyProfile.aspx.cs" Inherits="Placement_Portal.CreateCompanyProfile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <style>
        .wide{
            
            border:1px solid #456879;
	        border-radius:10px;
            width : 40%;
        }
        </style>
    <form id="form1" runat="server" style="height: 348px">
        <br />
        <br />
        <br />
        <asp:Label ID="CompanyIDLabel" runat="server" Text="Company ID : "></asp:Label>
        <br />
        <asp:TextBox ID="CompanyIDTextBox" runat="server" CssClass="wide"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="CompanyNameLabel" runat="server" Text="Company Name : "></asp:Label>
        <br />
        <asp:TextBox ID="CompanyNameTextBox" runat="server" CssClass = "wide"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="CompanyWebsiteLabel" runat="server" Text="Company Website : "></asp:Label>
        <br />
        <asp:TextBox ID="CompanyWebsiteTextBox" runat="server" CssClass ="wide"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="CompanyWebsiteLabel0" runat="server" Text="Password "></asp:Label>
        <br />
        <asp:TextBox ID="PasswordTextBox" runat="server" CssClass ="wide"></asp:TextBox>
        <br />
        <br />
        <br />
        <asp:Button ID="CreateCompanyProfileButton" runat="server" Height="32px" Text="Create Company Profile" Width="164px" OnClick="CreateCompanyProfileButton_OnClick" />
        <br />
        <br />
        <asp:Label ID="Label1" runat="server"></asp:Label>
        <br />
        <br />
        <asp:Button ID="BackButton" runat="server" Text="Back" OnClick="BackButton_OnClick" />
        <br />
    </form>
</asp:Content>
