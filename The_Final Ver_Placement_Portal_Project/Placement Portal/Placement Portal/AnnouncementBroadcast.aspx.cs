﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.DynamicData;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace Placement_Portal
{
    public partial class AnnouncementBroadcast : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            Page.Theme = (string)Session["Theme"];
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void AnnounceClick(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source = (localdb)\MSSQLlocalDB; Initial Catalog = Placement_Database; Integrated Security = True";
            SqlCommand cmd = new SqlCommand("INSERT into Announcements(Announcement_ID, Description, Date) VALUES (@aid, @desc, @date)", con);

            int temp_num;
            int.TryParse(AnnouncementIDTextBox.Text.ToString(), out temp_num);
            cmd.Parameters.AddWithValue("@aid", temp_num);
            cmd.Parameters.AddWithValue("@desc", AnnouncementTextBox.Text.ToString());
            cmd.Parameters.AddWithValue("@date", AnnounceDateTextBox.Text.ToString());

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                Label1.Text = "Profile Created Successfully!!";

            }
            catch (Exception err)
            {
                Label4.Text = err.ToString();
            }
            finally
            {
                con.Close();
            }
        }

        protected void BackButton_OnClick(object sender,EventArgs e)
        {
            Response.Redirect("AdminMain.aspx");
        }
    }
}