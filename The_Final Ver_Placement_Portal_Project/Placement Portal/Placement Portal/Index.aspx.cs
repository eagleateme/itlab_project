﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Placement_Portal
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void student_Click(object sender, EventArgs e)
        {
            if (DropDownList1.SelectedValue == "Red")
                Session["Theme"] = "Skin1";
            else
                Session["Theme"] = "Skin2";

            Response.Redirect("Student.aspx");
        }

        protected void admin_Click(object sender, EventArgs e)
        {
            if (DropDownList1.SelectedValue == "Red")
                Session["Theme"] = "Skin1";
            else
                Session["Theme"] = "Skin2";

            Response.Redirect("Admin.aspx");
        }

        protected void ProfileCreatebtn_Click(object sender, EventArgs e)
        {
            if (DropDownList1.SelectedValue.ToString().Equals("Red"))
                Session["Theme"] = "Skin1";
            else
                Session["Theme"] = "Skin2";

            Response.Redirect("CreateProfile.aspx");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if(DropDownList1.SelectedValue == "Red")
                Session["Theme"] = "Skin1";
            else
                Session["Theme"] = "Skin2";

            Response.Redirect("Recruiter.aspx");
        }
    }
}