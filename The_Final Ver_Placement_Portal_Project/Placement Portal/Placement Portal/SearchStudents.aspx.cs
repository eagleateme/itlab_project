﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace Placement_Portal
{
    public partial class SearchStudents : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            Page.Theme = (string)Session["Theme"];
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source = (localdb)\MSSQLlocalDB; Initial Catalog = Placement_Database; Integrated Security = True";
            //Obtain the field desired field to check and modify query based on that
            SqlCommand cmd;
            try
            {
                switch (DropDownList1.SelectedItem.ToString())
                {
                    case "All":
                        cmd = new SqlCommand("Select * FROM Student_Details", con);
                        break;
                    case "Gender":
                        cmd = new SqlCommand("SELECT * FROM Student_Details where Gender = @gender", con);
                        cmd.Parameters.AddWithValue("@gender", TextBox1.Text);
                        break;
                    case "Branch":
                        cmd = new SqlCommand("SELECT * FROM Student_Details where Branch = @branch", con);
                        cmd.Parameters.AddWithValue("@branch", TextBox1.Text);
                        break;
                    case "Minimum CGPA":
                        cmd = new SqlCommand("SELECT * FROM Student_Details where CGPA > @cgpa", con);
                        cmd.Parameters.AddWithValue("@cgpa", TextBox1.Text);
                        break;
                    case "Group":
                        cmd = new SqlCommand("SELECT * FROM Student_Details, Groups WHERE Groups.Group_ID = Student_Details.Group_ID AND Groups.Group_Name = @group", con);
                        cmd.Parameters.AddWithValue("@group", TextBox1.Text);
                        break;
                    default:
                        Console.Write(DropDownList1.SelectedItem.ToString());
                        throw new Exception("Unable to recognize DropDownList Input");
                }

                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                ResultsLabel.Text = "";
                if (reader.HasRows)
                {
                    GridView1.DataSource = reader;
                    GridView1.DataBind();
                    while (reader.Read())
                    {
                        ResultsLabel.Text += reader["Student_ID"] + " : " + reader["Name"] + ";\n";
                    }
                }
                else
                    ResultsLabel.Text += "No Results found";
            }

            catch (Exception err)
            {
                ResultsLabel.Text = err.ToString();
            }
            finally
            {
                con.Close();
            }
        }
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            e.Row.Cells[48].Visible = false;
        }

        protected void BackButton_OnClick(object sender, EventArgs e)
        {
            if (Session["recruiter_ID"] == null)
                Response.Redirect("AdminMain.aspx");
            else
                Response.Redirect("RecruiterMain.aspx");
        }
    }
}