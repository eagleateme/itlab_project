﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.DynamicData;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.SqlTypes;


namespace Placement_Portal
{
    public partial class JobReg : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            Page.Theme = (string)Session["Theme"];
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SqlConnection con = new SqlConnection();
                con.ConnectionString = @"Data Source = (localdb)\MSSQLlocalDB; Initial Catalog = Placement_Database ; Integrated Security = True ;";
                try
                {
                    con.Open();

                    //Command to update single-valued entries
                    SqlCommand cmd = new SqlCommand("SELECT * from Job_Profile where Eligible_Branch in (SELECT Branch from Student_Details where Student_ID=@stu_id);", con);
                    int curr_id = (int)Session["username"];
                    cmd.Parameters.AddWithValue("@stu_id",curr_id);

                    SqlDataReader reader = cmd.ExecuteReader();
                    GridView1.DataSource = reader;
                    GridView1.DataBind();

                }
                catch (Exception err)
                {

                }
                finally
                {
                    con.Close();
                }

            }
        }

        protected void RegisterFunc(object sender,EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source = (localdb)\MSSQLlocalDB; Initial Catalog = Placement_Database; Integrated Security = True";
            SqlCommand cmd = new SqlCommand("INSERT into Student_RegisteredCompanies (Student_ID, Job_ID) VALUES (@student_id, @job_id)", con);
            int curr_id = (int)Session["username"];
            cmd.Parameters.AddWithValue("@student_id", curr_id);
            cmd.Parameters.AddWithValue("@job_id", TextBox1.Text);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                Label3.Text = "Succesfull";


            }
            catch (Exception err)
            {
                Label3.Text = err.ToString();
            }
            finally
            {
                con.Close();
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("StudentMain.aspx");
        }
    }
}