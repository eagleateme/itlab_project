﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.DynamicData;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace Placement_Portal
{
    public partial class View_Details : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            Page.Theme = (string)Session["Theme"];
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            int curr_id = (int)Session["username"];

            if (!Page.IsPostBack)
            {
                SqlConnection con = new SqlConnection();
                con.ConnectionString = @"Data Source = (localdb)\MSSQLlocalDB; Initial Catalog = Placement_Database ; Integrated Security = True ;";
                try
                {
                    con.Open();

                    //Command to update single-valued entries
                    SqlCommand cmd = new SqlCommand("SELECT * FROM Student_Details WHERE Student_ID = @student_id", con);
                    cmd.Parameters.AddWithValue("@student_id", curr_id);
                    SqlDataReader reader = cmd.ExecuteReader();
                    reader.Read();

                    RegNoTextBox.Text = reader["Student_ID"].ToString();
                    NameTextBox.Text = reader["Name"].ToString();

                    GenderTextBox.Text = reader["Gender"].ToString();
                    EmailTextBox.Text = reader["Email"].ToString();
                    DOBTextBox.Text = reader["DOB"].ToString();
                    PhoneTextBox.Text = reader["Contact_Number"].ToString();
                    XthStandardTextBox.Text = reader["Tenth_Marks"].ToString();
                    XIIthStandardTextBox.Text = reader["Twelfth_Marks"].ToString();
                    BacklogsTextBox.Text = reader["Backlogs"].ToString();

                    SemTextBox1.Text = reader["Sem1_Grade"].ToString();
                    SemTextBox2.Text = reader["Sem2_Grade"].ToString();
                    SemTextBox3.Text = reader["Sem3_Grade"].ToString();
                    SemTextBox4.Text = reader["Sem4_Grade"].ToString();
                    SemTextBox5.Text = reader["Sem5_Grade"].ToString();
                    SemTextBox6.Text = reader["Sem6_Grade"].ToString();
                    SemTextBox7.Text = reader["Sem7_Grade"].ToString();
                    SemTextBox8.Text = reader["Sem8_Grade"].ToString();

                    FatherNameTextBox.Text = reader["Fathers_Name"].ToString();
                    MotherNameTextBox.Text = reader["Mothers_Name"].ToString();
                    FatherOccupationTextBox.Text = reader["Fathers_Occupation"].ToString();
                    MotherOccupationTextBox.Text = reader["Mothers_Occupation"].ToString();
                    XIISchoolTextBox.Text = reader["Twelfth_School"].ToString();

                    XSchoolTextBox.Text = reader["Tenth_School"].ToString();
                    XIIBoardTextBox.Text = reader["Twelfth_Board"].ToString();
                    XBoardTextBox.Text = reader["Tenth_Board"].ToString();
                    CitizenTextBox.Text = reader["Citizenship"].ToString();
                    CityTextBox.Text = reader["City"].ToString();
                    StateTextBox.Text = reader["State"].ToString();
                    AddressTextBox.Text = reader["Address"].ToString();
                    AadhaarTextBox.Text = reader["Aadhaar"].ToString();
                    BranchTextBox.Text = reader["Branch"].ToString();

                }
                catch (Exception err)
                {
                    
                }
                finally
                {
                    con.Close();
                }

            }
        }
        protected void BackButton_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("StudentMain.aspx");
        }
    }
}