﻿<%@ Page Title="Search Students" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="SearchStudents.aspx.cs" Inherits="Placement_Portal.SearchStudents" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <form id="form1" runat="server">
        <asp:Label ID="Label1" runat="server" Text="Select the field you want to search"></asp:Label>
&nbsp;students for<br />
        <asp:DropDownList ID="DropDownList1" runat="server">
            <asp:ListItem Value="0">Gender</asp:ListItem>
            <asp:ListItem Value="1">Branch</asp:ListItem>
            <asp:ListItem Value="2">Minimum CGPA</asp:ListItem>
            <asp:ListItem Selected="True" Value="3">Group</asp:ListItem>
            <asp:ListItem>All</asp:ListItem>
        </asp:DropDownList>
        <br />
        Select the value you want to match<br />
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        <br />
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Search" />
        <br />
        <asp:Label ID="ResultsLabel" runat="server"></asp:Label>
        <asp:GridView ID="GridView1" runat="server" onrowdatabound="GridView1_RowDataBound">
        </asp:GridView>
        <br />
        <br />
        <br />
        <asp:Button ID="BackButton" runat="server" Text="Back" OnClick="BackButton_OnClick" />
        <br />
    </form>
</asp:Content>
