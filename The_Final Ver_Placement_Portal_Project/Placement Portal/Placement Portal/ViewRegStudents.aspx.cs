﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.DynamicData;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace Placement_Portal
{
    public partial class ViewRegStudents : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            Page.Theme = (string)Session["Theme"];
        }

        int curr_id;
        DataSet ds = new DataSet();
        protected void Page_Load(object sender, EventArgs e)
        {
            curr_id = (int)Session["recruiter_ID"];
            if (!IsPostBack)
            {
                SqlConnection con = new SqlConnection();
                con.ConnectionString = @"Data Source =  (localdb)\MSSQLLocalDB; Initial Catalog = Placement_Database; Integrated Security = True; Pooling  = False;";

                con.Open();
                SqlCommand command = new SqlCommand("select Company_ID from Recruiter WHERE Recruiter_ID = @rec_id", con);
                command.Parameters.AddWithValue("@rec_id", curr_id.ToString());
                SqlDataReader reader = command.ExecuteReader();
                reader.Read();
                string comp_id = ((int)reader["Company_ID"]).ToString();
                reader.Close();
                command.CommandText = "select * from Student_RegisteredCompanies sr INNER JOIN Student_Details s ON s.Student_ID = sr.Student_ID INNER JOIN Job_Profile j ON j.Job_ID = sr.Job_ID WHERE j.Company_ID = @c_id;";
                command.Parameters.AddWithValue("@c_id", comp_id);

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(ds, "Student_Profile");
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }

        }

        protected void BackButton_OnClick(object sender, EventArgs e)
        {

            Response.Redirect("RecruiterMain.aspx");
        }
    }
}