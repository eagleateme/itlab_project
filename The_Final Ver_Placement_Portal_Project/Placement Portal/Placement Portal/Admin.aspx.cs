﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Placement_Portal
{
    public partial class Admin : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            Page.Theme = (string)Session["Theme"];
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Session["recruiter_ID"] = null;
            Session["username"] = null;
            if(!IsPostBack)
            {
               
            }
        }

        protected void LoginButton_Click(object sender, EventArgs e)
        {
            if(UsernameTextBox.Text.Equals("Admin") && PasswordTextBox.Text.Equals("abc123"))
            {
                Response.Redirect("AdminMain.aspx");
            }
            else
            {
                ErrorLabel.Text = "Invalid Login Credentials";
            }
        }
        protected void BackButton_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("Index.aspx");
        }
    }
}