﻿<%@ Page Title="Contact Us" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="ContactDetails.aspx.cs" Inherits="Placement_Portal.ContactDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="form1" runat="server">
        <asp:Label ID="Label1" runat="server" Text="Contact details:"></asp:Label>
        <br />
        <br />
        <asp:Label ID="Label2" runat="server" Text="Email address:  eagleateme@gmail.com"></asp:Label>
        <br />
        <asp:Label ID="Label3" runat="server" Text="Phone Number: 9573259861"></asp:Label>
        <br />
        <br />
        <asp:Label ID="Label4" runat="server" Text="All feedback is appreciated"></asp:Label>
        <br />
        <br />
        <br />
        <asp:Button ID="Back" runat="server" OnClick="Back_Click" Text="Back" />
    </form>

</asp:Content>
