﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.DynamicData;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace Placement_Portal
{
    public partial class DeleteStudentAccount : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            Page.Theme = (string)Session["Theme"];
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_OnClick(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source = (localdb)\MSSQLlocalDB; Initial Catalog = Placement_Database; Integrated Security = True";
            SqlCommand cmd = new SqlCommand("DELETE FROM Student_Internship where Student_Id= @stu_id", con);

            int temp_num;
            int.TryParse(DeleteTextBox.Text.ToString(), out temp_num);
            cmd.Parameters.AddWithValue("@stu_id", temp_num);

          
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();

                cmd.CommandText = "DELETE FROM Student_Project where Student_Id= @stu_id";
                cmd.ExecuteNonQuery();

                cmd.CommandText = "DELETE FROM Student_Profile where Student_Id= @stu_id";
                cmd.ExecuteNonQuery();

                cmd.CommandText = "DELETE FROM Student_RegisteredCompanies where Student_Id= @stu_id";
                cmd.ExecuteNonQuery();

                cmd.CommandText = "DELETE FROM Student_Details where Student_Id= @stu_id";
                cmd.ExecuteNonQuery();
                Label3.Text = "Successful";


            }
            catch (Exception err)
            {
                //Debugging Purposes
                Label3.Text = err.ToString();
            }
            finally
            {
                con.Close();
            }

        }

        protected void BackButton_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("AdminMain.aspx");
        }
    }
}