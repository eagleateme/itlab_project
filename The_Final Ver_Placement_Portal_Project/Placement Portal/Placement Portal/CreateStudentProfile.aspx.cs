﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.DynamicData;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.SqlTypes;


namespace Placement_Portal
{
    public partial class CreateStudentProfile : System.Web.UI.Page
    {
        int curr_id;
        protected void Page_PreInit(object sender, EventArgs e)
        {
            Page.Theme = (string)Session["Theme"];
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            curr_id = (int)Session["username"];

        }
        protected void CreateStudentProfile_OnClick(object sender,EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source = (localdb)\MSSQLlocalDB; Initial Catalog = Placement_Database; Integrated Security = True";
            SqlCommand cmd = new SqlCommand("INSERT into Student_Profile(Profile_Type, Student_ID, Skills,Achievements,Related_Projects) VALUES (@prof_type, @student_id, @skills,@ach,@relproj)", con);
            cmd.Parameters.AddWithValue("@prof_type",ProfileTypeTextBox.Text);
            cmd.Parameters.AddWithValue("@student_id", curr_id);
            cmd.Parameters.AddWithValue("@skills",SkillsTextBox.Text);
            cmd.Parameters.AddWithValue("@ach", AchievementsTextBox.Text);
            cmd.Parameters.AddWithValue("@relproj",RelatedProjectsTextBox.Text);
                                 
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                Label2.Text = "Successful";


            }
            catch (Exception err)
            {
                Label2.Text = err.ToString();
            }
            finally
            {
                con.Close();
            }
        }
        protected void BackButton_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("StudentMain.aspx");
        }
    }
}