﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace Placement_Portal
{
    public partial class ViewRegisteredJobs : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            Page.Theme = (string)Session["Theme"];
        }

        int curr_id;
        DataSet ds = new DataSet();
        protected void Page_Load(object sender, EventArgs e)
        {
            curr_id = (int)Session["username"];
            if (!IsPostBack)
            {
                SqlConnection con = new SqlConnection();
                con.ConnectionString = @"Data Source =  (localdb)\MSSQLLocalDB; Initial Catalog = Placement_Database; Integrated Security = True; Pooling  = False;";

                SqlCommand command = new SqlCommand("select * from Student_RegisteredCompanies where Student_Id = @student_id;", con);
                command.Parameters.AddWithValue("@student_id", curr_id);

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(ds, "Student_RegisteredCompanies");
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }

        }
        protected void BackButton_OnClick(object sender, EventArgs e)
        {

            Response.Redirect("StudentMain.aspx");
        }
    }
}