﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.DynamicData;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.SqlTypes;


namespace Placement_Portal
{
    public partial class CreateStudentGroups : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            Page.Theme = (string)Session["Theme"];
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void CreateGroupButton_OnClick(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source=(localdb)\MSSQLlocalDB; Initial Catalog = Placement_Database; Integrated Security = True";
            SqlCommand cmd = new SqlCommand("INSERT into Groups (Group_ID,Group_Description,Group_Admin,Group_Name) VALUES (@gid, @g_desc, @g_admin,@gname)", con);

            int temp_num;
            int.TryParse(GroupIDTextBox.Text.ToString(), out temp_num);
            cmd.Parameters.AddWithValue("@gid", temp_num);
            cmd.Parameters.AddWithValue("@g_desc", GroupDescriptionTextBox.Text.ToString());
            int.TryParse(GroupAdminTextBox.Text.ToString(), out temp_num);
            cmd.Parameters.AddWithValue("@g_admin", temp_num);
            cmd.Parameters.AddWithValue("@gname", GroupNameTextBox.Text.ToString());


            try
            {
                con.Open();
                cmd.ExecuteNonQuery();

                Label1.Text = "Group Created Successfully!!";

           
            cmd.CommandText = "UPDATE Student_Details SET Group_Id = @gid where Student_Id = @stu_id";
            cmd.Parameters.AddWithValue("@stu_id", GroupAdminTextBox.Text.ToString());

            cmd.ExecuteNonQuery();
            Label7.Text = "Successful";


            }
            catch (Exception err)
            {
                Label7.Text = err.ToString();
            }

            finally
            {
                con.Close();
            }
        }

        protected void AddMembersButton_OnClick(object sender, EventArgs e)
        {

            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source = (localdb)\MSSQLlocalDB; Initial Catalog = Placement_Database; Integrated Security = True";
            SqlCommand cmd = new SqlCommand("UPDATE Student_Details SET Group_Id= @gid where Student_Id = @stu_id", con);

            int temp_num;
            int.TryParse(GroupIDTextBox.Text.ToString(), out temp_num);
            cmd.Parameters.AddWithValue("@gid", temp_num);

            int.TryParse(AddMembersTextBox.Text.ToString(), out temp_num);
            cmd.Parameters.AddWithValue("@stu_id", temp_num);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception err)
            {
                //Debugging Purposes
                Label7.Text = err.ToString();
            }
            finally
            {
                con.Close();
            }
        }

        protected void DeleteMembersButton_OnClick(object sender, EventArgs e)
        {

            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source = (localdb)\MSSQLlocalDB; Initial Catalog = Placement_Database; Integrated Security = True";
            SqlCommand cmd = new SqlCommand("UPDATE Student_Details SET Group_Id = NULL where Student_Id=@stu_id", con);

            int temp_num;
            int.TryParse(DeleteMembersTextBox.Text.ToString(), out temp_num);
            cmd.Parameters.AddWithValue("@stu_id", temp_num);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception err)
            {
                //Debugging Purposes
                Label7.Text = err.ToString();
            }
            finally
            {
                con.Close();
            }

        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source = (localdb)\MSSQLlocalDB; Initial Catalog = Placement_Database; Integrated Security = True";
            SqlCommand cmd = new SqlCommand("UPDATE Groups SET Group_Admin = @gadmin where Group_ID=@gid", con);

            int temp_num;
            int.TryParse(GroupIDTextBox.Text.ToString(), out temp_num);
            cmd.Parameters.AddWithValue("@gid", temp_num);
            cmd.Parameters.AddWithValue("@gadmin",GroupAdminTextBox.Text.ToString());

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception err)
            {
                //Debugging Purposes
                Label7.Text = err.ToString();
            }
            finally
            {
                con.Close();
            }

        }
        protected void BackButton_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("AdminMain.aspx");
        }
    }
}