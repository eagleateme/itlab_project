﻿<%@ Page Title="Admin Login" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Admin.aspx.cs" Inherits="Placement_Portal.Admin" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="admin_form" runat = "server" >

    <div style="text-align : center" >    
        <br />
    
        <asp:Label ID="UserNameLabel" runat="server" Text="Username : "></asp:Label>
        <asp:TextBox  ID="UsernameTextBox" runat="server"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="PasswordLabel" runat="server" Text="Password : "></asp:Label>
        <asp:TextBox  ID="PasswordTextBox" TextMode="Password" runat="server"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="ErrorLabel" runat="server" ForeColor="Red"></asp:Label>
        <br />
        <br />
        <br />
        <asp:Button ID="LoginButton" runat="server" Text="Login" OnClick="LoginButton_Click" />
        <br />
        <br />
        <asp:Button ID="BackButton" runat="server" Text="Back" OnClick="BackButton_OnClick" />
        <br />
    </div>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <br />
        <br />
        <br />
        <asp:Button ID="Button1" runat="server" Text="Back" OnClick="BackButton_OnClick" />
        <br />
    </form>
    
</asp:Content>
